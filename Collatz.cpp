// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream


using namespace std;

int cache[1000001];

// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}


// ------------
// collatz_eval
// Recursive function to find the collatz cycle length for one num.
// ------------
int recurse (long temp, int count) {
    if (temp != 1) {
        // Use a cache to check if cycle has already been computed.
        if (temp < 1000000 && cache[temp] != 0) {
            return cache[temp];
        }
        if (temp % 2 == 0) {
            count = recurse(temp / 2, count);
        }
        else {
            count = recurse(temp * 3 + 1, count);
        }
    }
    count++;
    if (temp < 1000000)
        cache[temp] = count;
    return count;
}


// Find the largest Collatz cycle between i and j inclusive.
int collatz_eval (int i, int j) {
    assert (i < 1000000 && i > 0);
    assert (j < 1000000 && i > 0);

    // Swap i and j if they are in opposite order.
    if (i > j) {
        int tempo = i;
        i = j;
        j = tempo;
    }

    // Find the max cycle length for all the numbers in range.
    int max = 0;
    for (int x = i; x < j + 1; x++) {
        long temp = x;
        int count = recurse (temp, 0);
        if (count > max) {
            max = count;
        }
    }

    return max;
}


// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read (r, i, j)) {
        const int v = collatz_eval (i, j);
        collatz_print (w, i, j, v);
    }
}
